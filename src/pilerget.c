/*
 * pilerget.c, SJ
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <locale.h>
#include <syslog.h>
#include <piler.h>
#include <getopt.h>

extern char *optarg;
extern int optind;

void usage(){
   printf("usage: pilerget <piler-id>\n");
   printf("    [-c <config file>]                Config file to use if not the default\n");
}

int main(int argc, char **argv){
   int readkey=1, c;
   char *configfile=CONFIG_FILE;
   struct session_data sdata;
   struct __data data;
   struct __config cfg;


   /*if(argc < 2){
      usage();
      exit(1);
   }*/

   while(1) {
#ifdef _GNU_SOURCE
      static struct option long_options[] =
         {
            {"config",       optional_argument,  0,  'c' },
            {"help",         no_argument,        0,  'h' },
            {0,0,0,0}
         };

      int option_index = 0;

      c = getopt_long(argc, argv, "c:h?", long_options, &option_index);
#else
      c = getopt(argc, argv, "c:h?");
#endif
   
      if(c == -1) break;
   
      switch(c){
         case 'c' : 
   	         configfile = optarg;
   		 break;
         case '?':
		 usage();
   		 exit(0);
         default:
   		 break;
      }
   }

   (void) openlog("pilerget", LOG_PID, LOG_MAIL);

   cfg = read_config(configfile);

   if(argc >= 3){
      readkey = 0;
      cfg.encrypt_messages = 0;
   }

   if(readkey == 1 && read_key(&cfg)){
      printf("%s\n", ERR_READING_KEY);
      return 1;
   }


   if(open_database(&sdata, &cfg) == ERR) return 0;


   snprintf(sdata.ttmpfile, SMALLBUFSIZE-1, "%s", argv[1]);
   snprintf(sdata.filename, SMALLBUFSIZE-1, "%s", sdata.ttmpfile);
   retrieve_email_from_archive(&sdata, &data, stdout, &cfg);


   close_database(&sdata);

   return 0;
}


